package com.tompai.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class SpringElfinderApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(SpringElfinderApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringElfinderApplication.class, args);
		System.out.println("管理所有的目录: http://localhost:8181/?token=mytoken#elf_A_");
		System.out.println("管理指定的目录: 添加参数appkey（yml文件配置）,http://localhost:8181/?appkey=elfinder1&token=mytoken#elf_A_");
		System.out.println("管理所有的目录: http://localhost:8181/elfinder/dist/#/?appkey=elfinder1#elf_A_");
		System.out.println("管理所有的目录: http://localhost:8181/elfinder/dist/#/table?appkey=elfinder1#elf_A_");

	}
}
