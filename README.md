# 基于Spring-boot2 和 elfinder 的在线Web文件管理系统
  基于Spring-boot2 和 elfinder 的在线Web文件管理系统，可以作为在线网盘、云盘使用；

### 效果图
 <p align="center">
  <img width="900" src="http://tigerxue.gitee.io/liinux-images/docs/fileupload/disk2.gif?raw=true">
 </p>

---

### Elfinder原有功能
- 支持在线文件下载
- 支持目录上传
- 支持zip tar Gzip 的在线解压和压缩文件夹
- 支持多种文本格式的高亮显示和在线编辑
- 支持在线文件预览
- 支持文件夹权限设置
- 支持国际化
---

### 本项目新增功能
* [x] 增加了token验证，必须token正确才能访问elfinder进行文件管理
* [x] 增加了appkey，多个不同目录时可以设置只显示管理其中的一个目录
* [x] 增加了获取文件列表的接口，可用于构建供外部用户使用的下载页面

### 本项目后期功能
* [ ] 增加前端登录界面
* [ ] 增加多用户权限控制功能
* [ ] 增加文件链接分享功能
* [ ] 增加Office文档在线查看功能
* [ ] ......
---

### 配置 application.yml
```
token: mytoken
server:
  port: 8181
  tomcat:
    uri-encoding: UTF-8
    connection-timeout: 20000
    max-threads: 100
    accept-count: 200
spring:
  mvc:
    view:
      prefix: /
      suffix: .html
  resources:
    static-locations: classpath:static/
  servlet:
    multipart:
      max-file-size: -1
      max-request-size: -1      
  http:
    encoding:
      charset: utf-8
      force: true
      enabled: true
file-manager:
     thumbnail:
        width: 80 # 缩略图宽
     volumes:
        - Node:
          appkey: elfinder1
          source: fileSystem # 暂时只支持本地文件系统
          alias: elfinder1 # 目录别名
          path: /data/elfinder1 # 映射目录   D:/elfinder
          host: http://localhost:8899 #与映射目录下的相对路径拼接，host可以设置为相对地址，如/download
          isdefault: true # 是否默认打开
          locale:
          constraint:
            locked: false # 文件夹是否锁定
            readable: true # 是否可读
            writable: true # 是否可写
        - Node:
          appkey: elfinder2
          source: fileSystem # 暂时只支持本地文件系统
          alias: elfinder2 # 目录别名
          path: /data/elfinder2 # 映射目录   /D:/elfinder
          host: http://localhost:8898 #与映射目录下的相对路径拼接，host可以设置为相对地址，如/download
          isdefault: true # 是否默认打开
          locale:
          constraint:
            locked: false # 文件夹是否锁定
            readable: true # 是否可读
            writable: true # 是否可写
```

---

### 运行
#### 下载
```
git clone git@gitee.com:tigerxue/springboot-elfinder.git
```
#### mvn
```
	cd springboot-elfinder
    mvn install -Dmaven.test.skip=true
    mvn spring-boot:run
```
#### eclipse
```
    eclipse打开项目springboot-elfinder
    springboot-elfinder 右键项目选择Run As -> Maven Install
    spring-web 右键项目选择Run As -> Spring Boot App

```

访问地址：
- 管理所有的目录 http://localhost:8181/?token=mytoken#elf_A_
- 管理指定的目录 添加参数appkey（yml文件配置），http://localhost:8181/?appkey=elfinder1&token=mytoken#elf_A_

---
### war包部署运行
#### 下载
[war包下载页面](https://gitee.com/tigerxue/springboot-elfinder/attach_files/368328/download) -> Download -> Assets -> springboot2-elfinder-web-1.0.war

#### 部署运行
- 将war包放入到tomcat的webapps目录下，启动tomcat即可

#### tomat部署访问地址
1. 需要带上项目名称elfinder
- 管理所有的目录 http://localhost:8181/?token=mytoken#elf_B_dG1w
- 管理指定的目录 添加参数appkey（yml文件配置），http://localhost:8181/?appkey=elfinder1&token=mytoken#elf_A_

#### 联系作者

- [oschina](http://git.oschina.net/liinux)
- [cnblogs](http://www.cnblogs.com/liinux)
- [github](https://github.com/liinnux)

#### 扫码关注
<table>
    <tr>
		<td>作者微信</td>
        <td><img src="http://tigerxue.gitee.io/liinux-images/docs/img/alukesi.jpg" /></td>
        <td>公众号</td>
        <td><img src="http://tigerxue.gitee.io/liinux-images/docs/img/ghosy-login-微信公众号.jpg" /></td>
        <td>企鹅群</td>
        <td><img src="http://tigerxue.gitee.io/liinux-images/docs/img/ghosy-login-QQ群.jpg" /></td>
    </tr>
</table>

